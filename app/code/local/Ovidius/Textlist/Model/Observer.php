<?php
class Ovidius_Textlist_Model_Observer
{
    /* Insert block programically in layout - right sidebar */

    public function controllerActionLayoutGenerateBlocksAfter($observer)
    {
        $layout = $observer->getLayout();
        $targetBlock = $layout->getBlock('right');  // "right" - block's name in layout xml
        
        if ($targetBlock) {
            $dummyBlock = $layout->createBlock('ovidius_textlist/dummy')
                ->setTemplate('textlist/dummy.phtml')
                ->setHeading('Dummy heading')
                ->setBody('This block was inserted via observer');

            $targetBlock->insert($dummyBlock);
        }

    }
}
